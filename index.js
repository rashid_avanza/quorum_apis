var Web3 = require('web3');

const config = {
    host: "avanzamgc.westindia.cloudapp.azure.com",
    port: 8545, // was 8545
    network_id: "*", // Match any network id
    gasPrice: 0,
    gas: 4500000
};


const streamingConfig = {
    host: 'http://avanzamgc.westindia.cloudapp.azure.com:4000/cakeshop/ws'
}
var web3 = web3 = new Web3(new Web3.providers.HttpProvider('http://'+config.host+":"+config.port));
const defaultAddress = web3.eth.coinbase;
/*
//Getting node Information
web3.version.getNode(function(error, result){ 
    //console.log("Node Informatormation:",result)
});

//Get network Id
web3.version.getNetwork(function(error, result){ 
   //console.log("Network Id:",result)
});

//Get number of peers
web3.net.getPeerCount(function(error, result){ 
   //console.log("Number of Peers:",result)
});

//Get account public keys
web3.eth.getAccounts(function(error, result){ 
    //console.log("Accounts:",result)
});

//Get account public keys
web3.eth.getBlockNumber(function(error, result){ 
    //console.log("Current Block:",result)    
});

//Return balance of an account
web3.eth.getBalance(defaultAddress, function(error, result){
    //console.log('Balance:',result)
});

//Return content of a block
const blockNumber = 1000;
const returnTransactionObjects = true;
web3.eth.getBlock(blockNumber, returnTransactionObjects, function(error, result){
    //console.log('Block content: ',result)
});


//Return number of transactions
web3.eth.getBlockTransactionCount(blockNumber, function(error, result){
    console.log('Tx count: ',result)
});
//Gets code stored at a specific address
const exampleContract = '0x8e8f48f627ee5efffac3b1d8c28bf978c69b3150';
web3.eth.getCode(exampleContract, function(error, result){
    //console.log(result)
});



*/

//Compile a contract and send transaction
async function compileAndGetContract(code){
    try{ 
        const contract = await  web3.eth.compile.solidity(code);
        let contractCode = '';
        let contractAbi = [];
        for(var k in contract){
            contractCode = contract[k].code;
            contractAbi = contract[k].info.abiDefinition;
        }
        const contractInstance = await web3.eth.contract(contractAbi);        
        const contractObject = {
            instance: contractInstance,
            code: contractCode
        };

        return contractObject;
    } catch (err){
        console.log(err)
    }
}

function deployContract(instance, params, from, code){
    return new Promise((resolve,reject) => {
        instance.new(params, {
            data: code,
            from: from,
            gas: 90000*2
        },async (err, result) => {
            if (err) {
                reject(err);
            } else {
                const txObject = web3.eth.getTransaction(result.transactionHash);
                let txReceipt = await web3.eth.getTransactionReceipt(result.transactionHash);

                while (txReceipt===null){
                    txReceipt =  await web3.eth.getTransactionReceipt(result.transactionHash);
                }
                const contractAddress = txReceipt.contractAddress;
                resolve(contractAddress);  
            }
        });
    })
}

var fs = require('fs');
fs.readFile('contract.sol', 'utf8', async function(err, contents) {
    //Compiling code
    const contractObject =  await compileAndGetContract(contents);
    const contractAddress = await deployContract(contractObject['instance'],23, defaultAddress, contractObject['code']);
    console.log("Contract Address:",contractAddress);
    const my = contractObject['instance'].at(contractAddress);
    //Watching for logs
    var filter = my.Change({}, function (error, result) {
        if (!error)
          console.log(result);
      });
    console.log(my.set(9, {from: web3.eth.coinbase}));
    console.log(my.get())
});


//var Stomp = require('stompjs')
//var SockJS = require('sockjs-client')
//var stomp = Stomp.over(new SockJS(streamingConfig['host']));
/*stomp.connect({},
    function(frame) {
        console.log("Connected via STOMP!");
        stomp.subscribe("/topic/metrics/txnPerSec", function(message) {
            //console.log(message.body)
        });
        
    },
    function(err) {
        console.log(err)
    }
);*/
  


var filter = web3.eth.filter('latest');

filter.watch(function (error, log) {
  console.log(log); //  {"address":"0x0000000000000000000000000000000000000000", "data":"0x0000000000000000000000000000000000000000000000000000000000000000", ...}
});


var filter2 = web3.eth.filter({address: defaultAddress, topics: ['initialized']});
filter2.get((err, log) => {
    console.log(err,log)
})